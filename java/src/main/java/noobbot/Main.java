package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonObject;
import com.google.gson.GsonBuilder;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        final Gson gson = new Gson();
        final JsonReader jreader = new JsonReader(reader);
        final JsonParser jparser = new JsonParser();
        CarIdentifier ourCar = new CarIdentifier();
        CarRace race = new CarRace();

        jreader.setLenient(true);

        // first thing: tell the server we're here
        send(join);

        // deal with all possible messages in on large super-loop
        // a bit crude, but it'll get the job done
        for(;;) {
            final JsonElement jElement = jparser.parse(jreader);
            if (!jElement.isJsonObject()) {
                // ignore anything that's not an object at this level, code doesn't know what to do with it anyway
                continue;
            }
            final JsonObject msgTopLevel = jElement.getAsJsonObject();
            final String msgType = msgTopLevel.get("msgType").getAsString();


            if (msgType == null ) {
                // System.out.println("NULL MESSAGE received");

            } else if (msgType.equals("join")) {  // 1
                // System.out.println("Joined");

            } else if (msgType.equals("yourCar")) {  // 2
                // need this to find out our car color
                ourCar = gson.fromJson(msgTopLevel.get("data"), CarIdentifier.class);
                // System.out.println("Your car is: " + ourCar.name + "  " + ourCar.color);

            } else if (msgType.equals("gameInit")) {  // 3
                // System.out.println("Race init");
                final GameInitData gameInit = gson.fromJson( msgTopLevel.get("data").getAsJsonObject(), GameInitData.class );
                race.onGameInit(gameInit.race.track.pieces, parseTrackLanes(gameInit.race.track.lanes) );
                // System.out.println("Track Segments = ");
                // System.out.println(race.track);
                send(new Ping());

            } else if (msgType.equals("gameStart")) {  // 5
                System.out.println("Race start");
                send(new Ping());

            } else if (msgType.equals("carPositions")) {
                // todo: there's an initial carPositions sent before game starts. i don't think it matters..
                int i;
                final CarPosition[] positions = gson.fromJson( msgTopLevel.get("data").getAsJsonArray(), CarPosition[].class );
                for (i = 0; i < positions.length; i++) {
                    if (positions[i].id.isSameAs(ourCar)) {
                        CarRace.CarRaceResponse reponse = race.onCarPosition(positions[i]);
                        if (reponse instanceof CarRace.CarRaceThrottle) {
                            send(new Throttle( ((CarRace.CarRaceThrottle)reponse).throttle ));
                        }
                        else if (reponse instanceof CarRace.CarRaceSwitch) {
                            send(new Switch( ((CarRace.CarRaceSwitch)reponse).direction ));
                        }
                        else {
                            send(new Ping());
                        }
                    }
                }

            } else if (msgType.equals("lapFinished")) {
                // final int lapCount = msgTopLevel.get("data").getAsJsonObject().get("lapTime").getAsJsonObject().get("lap").getAsInt();
                // final int lapTime = msgTopLevel.get("data").getAsJsonObject().get("lapTime").getAsJsonObject().get("millis").getAsInt();
                // System.out.println(" Laptime " + lapCount + " = " + lapTime + "msec");
                race.onLapFinish();

            } else if (msgType.equals("gameEnd")) {
                System.out.println("Game is over.");

            } else if (msgType.equals("tournamentEnd")) {
                // tournamentEnd contains no data; but it appears to be the last message we get from the server
                System.out.println("Tournament is over.");
                break;

            } else {
                // System.out.println("This just in:  " + msgType + ": " + msgTopLevel.get("data"));
                send(new Ping());

            }
        }  // core 'for' loop


        System.out.println("We are done here.");

    }  // end 'public Main'


    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }


    //
    // a little helper function to convert lane information in message to simple array
    //
    private Double[] parseTrackLanes(final TrackLane[] lanesArray) {
        List<Double> lanesList = new ArrayList<Double>();
        Double[] result = new Double[0];
        int index;

        for (index = 0; index < lanesArray.length; index++) {
            lanesList.add( lanesArray[index].index, new Double(lanesArray[index].distanceFromCenter) );
        }
        return lanesList.toArray(result);
    }

}  // end 'public class Main'



abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}


class JoinRace extends SendMsg {
    public final BotId botId;
    public final String trackName;
    public final int carCount;

    JoinRace(final String name, final String key, final String trackName) {
        this.botId = new BotId(name, key);
        this.trackName = trackName;
        this.carCount = 1;
    }

        public class BotId {
            public final String name;
            public final String key;

            BotId(final String name, final String key) {
                this.name = name;
                this.key = key;
            }
        }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}


class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class Switch extends SendMsg {
    private static final String RIGHT = "Right";
    private static final String LEFT = "Left";
    private final String direction;

    public Switch(final int directional) {
        if (directional > 0) {
            this.direction = RIGHT;
        }
        else {
            this.direction = LEFT;
        }
    }

    @Override
    protected Object msgData() {
        return direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
