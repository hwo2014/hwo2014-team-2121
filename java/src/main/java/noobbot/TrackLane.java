package noobbot;


//
// defines a lane, in the format sent over by the server
// is just a name/color pair, both strings
// we don't *do* a lot with these, other than instantiate and equate
//
public class TrackLane {
    public int distanceFromCenter;
    public int index;
}
