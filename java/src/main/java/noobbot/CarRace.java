package noobbot;


//
// run and manage an entire race
// i'll try to keep this free from details of communication protocol
// this thing gets some events and responds to them without knowing where the events come from or where its responses go
// 
// gets initial details about the race: track layout, lane information, etc.  (ignore other car information for now..)
//
public class CarRace {

    public enum DriveState {
        SHED_SPEED, CONTROLLED_SPEED, FULL_THROTTLE
    }

    private static final Double THROTTLE_MAX = 1.0;
    private static final Double THROTTLE_MIN = 0.0;
    private static final Double THROTTLE_CONTROL = 0.7;
    private static final double SLIP_MONITOR_MAX_ANGLE = 0.1;  // if car drift angle stays within +/- of this limit for SLIP_COUNT then current speed is safe speed for this turn
    private static final int SLIP_MONITOR_COUNT = 5;  // number of consecutive no-slip steps before we update race geometry
    private static final double SAFE_ENTRY_MARGIN = 1.0;  // leave a little margin in save entry speed calculation
    private static final Double DEFAULT_SAFE_GEOMETRY = 0.2;  // set default *way* low; car learns what's safe as it travels
    // the higher the factor, the longer the distance that CarRace thinks it needs
    private static final int SHED_FACTOR = 5;
    private static final Double CRASH_ANGLE = 60.0;


    public Track track;
    public Double throttle = THROTTLE_MAX;
    public CarPosition prevPosition = new CarPosition();
    public DriveState driveState = DriveState.FULL_THROTTLE;  // always start with full throttle, i think it's safest and can't hurt since we're surely not moving...
    public ShedPoint shedPoint = new ShedPoint();
    public int switchSent = -1;
    public int noSlipCount = 0;
    public double maxSafeGeometryFactor = DEFAULT_SAFE_GEOMETRY;
    public double maxDrift = 0.0;
    public double minDrift = 0.0;
    public double baseThrottle = 0.2;



    CarRace() {
        track = new Track();
    }


    CarRace(final TrackPiece[] pieces, final Double[] lanes) {
        this.track = new Track(pieces, lanes);
    }


    public void onGameInit(final TrackPiece[] pieces, final Double[] lanes) {
        this.track = new Track(pieces, lanes);
    }

    public CarRaceResponse onCarPosition(final CarPosition newPosition) {
        //
        // phase I - set throttle value
        //

        CarRaceResponse response = new CarRaceThrottle(0.2);  // create a safe default in case of some accident
 
        // 0. calculate current speed & spin
        double nowSpeed = track.getSimpleDistance(newPosition, prevPosition);
        double nowSpin = newPosition.getAngle() - prevPosition.getAngle();
        double remainingDrift = 0.0;
        double budgetRate = 0.0;

        //
        // monitor piece geometry - see if we can't feel out a higher safe entry speed based on what we see on course during racing...
        // want to get find the largest safe geometry factor for current track physics
        // caveats:
        // if current piece is a straight piece, don't do the calculation; it doesn't apply
        // note: i'm going to ignore switch pieces - a switch might just mislead the calculation. so, if start lane != end lane, start count over.
        //
        if (
            (!track.isStraight(newPosition.getPieceIndex()))
                &&
            (newPosition.getStartLaneIndex() == newPosition.getEndLaneIndex())
                &&
            ((newPosition.getAngle() < SLIP_MONITOR_MAX_ANGLE) && (newPosition.getAngle() > -SLIP_MONITOR_MAX_ANGLE))
        ) {
            noSlipCount++;
            if (noSlipCount > SLIP_MONITOR_COUNT) {
                // OK: we're more or less not slipping at this point => under current physics, current speed is a safe speed to enter a piece of current geometry
                double thisGeometryFactor = (nowSpeed * nowSpeed) / track.getRadius(newPosition.getPieceIndex(), newPosition.getStartLaneIndex());
                if (thisGeometryFactor > maxSafeGeometryFactor) {
                    // System.out.println("   new Geometry factor: " + String.format("%5.3f", thisGeometryFactor));
                    maxSafeGeometryFactor = thisGeometryFactor;
                }
            }
        }
        else {
            noSlipCount = 0;
        }


        // for convenience, point to current track piece
        TrackPiece nowPiece = track.getPiece(newPosition.getPieceIndex());

        // TODO: if there are any track notes, apply those now
 
        // do state machine in two stages: first decide what state to be in (change state in if necessary); then execute that state 
        switch(driveState) {
            case FULL_THROTTLE:
                shedPoint = findClosestShedPoint(newPosition, nowSpeed);
                if (shedPoint.distance <= 0) {
                    driveState = DriveState.SHED_SPEED;
                    // System.out.println("   entered " + driveState + " : " + shedPoint.speed + ", " + shedPoint.piece );
                }
                else if ( !track.isStraight(newPosition.getPieceIndex()) ) {
                    driveState = DriveState.CONTROLLED_SPEED;
                    throttle = THROTTLE_CONTROL;  // bump throttle upto this amount to make sure we keep rolling a little at start of controlled speed runs
                }
                break;
         
            case SHED_SPEED:
                // new: it was possible to go right through shed piece entirely above the shed speed => car got stuck in shed speed mode for upto a whole lap.
                // so: exit shed speed state on reaching shed piece
                if (shedPoint.piece == newPosition.getPieceIndex()) {
                    driveState = DriveState.CONTROLLED_SPEED;
                }
                break;
         
            case CONTROLLED_SPEED:
                shedPoint = findClosestShedPoint(newPosition, nowSpeed);
                if (shedPoint.distance <= 0) {
                    driveState = DriveState.SHED_SPEED;
                }
                else
                if (nowPiece.isStraight()) {
                    driveState = DriveState.FULL_THROTTLE;
                }
                break;
        }

        // second, do whatever actions the current state requires
        switch(driveState) {
            case SHED_SPEED:
                // brake hard; if we reach target speed ahead of curve, use simple bang-bang controller to keep speed close to shedPoint speed
                if (nowSpeed > shedPoint.speed) {
                    throttle = THROTTLE_MIN;
                }
                else {
                    // System.out.println("   bit-bang!" );
                    throttle = THROTTLE_CONTROL;
                }
                break;
         
            case CONTROLLED_SPEED:
                // figure out how many degrees of drift left before we crash
                if (nowPiece.getTurn() > 0) {
                    remainingDrift = (CRASH_ANGLE - newPosition.angle);
                }
                else {
                    remainingDrift = (CRASH_ANGLE + newPosition.angle);
                }
                // budgetRate = (drift available to spend) / (distance left in curve)
                double remainingDistInCurve =  track.getRemainingDistanceInCurve( newPosition.getPieceIndex() ) - newPosition.getInPieceDistance();
                budgetRate = remainingDrift / remainingDistInCurve;
                // nowRate = (this angle) - (last angle)

                if (nowPiece.getTurn() > 0) {
                    throttle =  baseThrottle + (0.5 * (budgetRate - nowSpin));
                }
                else {
                    throttle =  baseThrottle + (0.5 * (budgetRate + nowSpin));
                }

                // cap/trim throttle output to make sure it's a legal value
                if (Double.isNaN(throttle))  { throttle = THROTTLE_MIN;  }
                if (throttle < THROTTLE_MIN)  { throttle = THROTTLE_MIN; }
                if (throttle > THROTTLE_MAX)  { throttle = THROTTLE_MAX; }

                break;
         
            case FULL_THROTTLE:
                throttle = THROTTLE_MAX;
                break;
        }

        if (newPosition.getAngle() > maxDrift)  {  maxDrift = newPosition.getAngle(); }
        if (newPosition.getAngle() < minDrift)  {  minDrift = newPosition.getAngle(); }

        // System.out.println(" " + newPosition.getLap() + " : " + String.format("%2d", newPosition.getPieceIndex()) + " :" + String.format("%6.2f", newPosition.getInPieceDistance()) + 
        //     ";  angle = " + String.format("%+06.2f", newPosition.getAngle()) + 
        //     ";  speed = " + String.format("%5.2f", nowSpeed) +
        //     ";  spin = " + String.format("%+7.4f", nowSpin) + 
        //     ";  throttle = " + String.format("%4.2f", throttle) +
        //     ";  turn = " + String.format("%+6.1f", nowPiece.getTurn()) +
        //     ";  budget = " + String.format("%+6.2f", remainingDrift) +
        //     ";  budgetRate = " + String.format("%+6.2f", budgetRate) +
        //     ";  state = " + driveState +
        //     ";  maxDrift = " + String.format("%+06.2f", maxDrift) +
        //     ";  minDrift = " + String.format("%+06.2f", minDrift) +
        // "");

        // final exit actions: write any history CarRace needs to keep
        prevPosition = newPosition;

        response = new CarRaceThrottle(throttle);


        //
        // phase II - investigate whether we should switch or not (overrides throttle setting)
        //
        double remainingDistance = track.getLength(newPosition.getPieceIndex(), newPosition.getStartLaneIndex()) - newPosition.getInPieceDistance();
        if (remainingDistance < 50.0) {
            if (track.getPieceAfter(newPosition.getPieceIndex()).isSwitchPiece()) {
                if (newPosition.getPieceIndex() != switchSent) {
                    int direction = Math.random() > 0.5 ? -1 : 1;
                    // System.out.println("  Switching: " + direction);
                    response = new CarRaceSwitch(direction);
                    switchSent = newPosition.getPieceIndex();
                }
            }
        }


        //
        // phase III - investigate whether we should turbo or not
        //



        return response;
    }


    public void onLapFinish() {
            if (maxDrift < 30.0 && minDrift > -30.0) {
                if (baseThrottle < 0.75) {
                    baseThrottle += 0.15;
                }
            }
            else if (maxDrift < 40.0 && minDrift > -40.0) {
                if (baseThrottle < 0.8) {
                    baseThrottle += 0.02;
                }
            }

            System.out.println(
                ";  maxDrift = " + String.format("%+06.2f", maxDrift) +
                ";  minDrift = " + String.format("%+06.2f", minDrift) +
                ";  baseThrottle = " + baseThrottle +
            "");
    }


    // examine piece geometry and determine max safe entry speed into that piece
    // this version takes lane offset into account...
    private Double getMaxEntrySpeed(final int pieceIndex, final int laneIndex) {
        // set same slow speed for all shapes to start with
        // if piece is straight, make max speed as large as possible
        if (track.isStraight(pieceIndex)) {
            return Double.MAX_VALUE;
        }
        else {
            return SAFE_ENTRY_MARGIN * Math.sqrt(maxSafeGeometryFactor * track.getRadius(pieceIndex, laneIndex));
        }
    }



    // internal helper container class
    private class ShedPoint {
        private double distance;  // distance to shed point
        private double speed;  // speed car needs to get down to
        private int piece;  // the piece of track that the shedpoint belongs to

        ShedPoint() {
            this.distance = Double.MAX_VALUE;
            this.speed = Double.MAX_VALUE;
            this.piece = Integer.MAX_VALUE;
        }

        ShedPoint(final double distance, final double speed, final int piece) {
            this.distance = distance;
            this.speed = speed;
            this.piece = piece;
        }
    }


    //
    // looks ahead a few hundred units and calculates how far to travel before reaching closest shed point
    // (noting that its possible that closest shed point is not necessarily the shed point for the closest piece.)
    // returns distance to closest shed point.
    // if the distance is positive, there's some time left. if negative, time to shed speed
    // New: ignores track pieces immediately in front of car that are part of current curve
    //
    private ShedPoint findClosestShedPoint(final CarPosition currentPosition, final double currentSpeed) {
        // for each one that has an entry condition, calculate the shedPoint for that entry point
        // if closer than closest known, then that's the one we worry about

        int i;
        double shedSpeed;
        double shedDistance;
        double shedPoint;
        double distanceAhead = -currentPosition.getInPieceDistance();
        // don't need to look further ahead than distance it takes to come to a complete stop
        final double maxLookahead = (currentSpeed * currentSpeed) * SHED_FACTOR;
        ShedPoint closestShedPoint = new ShedPoint();

        // for purposes of shedding correctly, need to skip over pieces with same curve as current piece...
        for (i = currentPosition.getPieceIndex(); distanceAhead < maxLookahead; i = track.getIndexAfter(i) ) {
            if (!track.sameCurvature( currentPosition.getPieceIndex(), track.getIndexAfter(i) )) {
                break;
            }
            distanceAhead += track.getLength(i, currentPosition.getStartLaneIndex());
        }

        for (; distanceAhead < maxLookahead; i = track.getIndexAfter(i) ) {
            distanceAhead += track.getLength(i, currentPosition.getStartLaneIndex());

            shedSpeed = this.getMaxEntrySpeed(track.getIndexAfter(i), currentPosition.getStartLaneIndex());
            if (currentSpeed > shedSpeed) {
                // may need to shed speed to safely enter piece #i - let's see:
                shedDistance = ((currentSpeed * currentSpeed) - (shedSpeed * shedSpeed)) * SHED_FACTOR;
                shedPoint = distanceAhead - shedDistance;
                if (shedPoint < closestShedPoint.distance) {
                    closestShedPoint = new ShedPoint(shedPoint, shedSpeed, track.getIndexAfter(i));  // use index after, b/c that's the piece the condition belongs to.
                }
            }
        }

        // return distance remaining to closest shedpoint. if 0 or just a little negative, then you've just reached the shedpoint. will need to enter shed speed mode.
        // return minShedPoint;  // distance to most closest shedpoint (may not be closest entry point)
        return closestShedPoint;
    }


        //
        // empty sub-class: response to car positions message will be a child class of this
        //
        abstract public class CarRaceResponse {

        }

        public class CarRaceThrottle extends CarRaceResponse {
            public double throttle = THROTTLE_MIN;

            CarRaceThrottle(final double throttle) {
                this.throttle = throttle;
            }
        }

        public class CarRaceSwitch extends CarRaceResponse {
            public int direction = 0;

            CarRaceSwitch(final int direction) {
                this.direction = direction;
            }
        }

}

