package noobbot;


//
// idendifies a car during a tournament or a race
// is just a name/color pair, both strings
// we don't *do* a lot with these, other than instantiate and equate
//
/*
{
  "name": "Schumacher",
  "color": "red"
}
*/
public class CarIdentifier {
    public String name;
    public String color;

    CarIdentifier() {
        this.name = null;
        this.color = null;
    }

    CarIdentifier(final String name, final String color) {
        this.name = name;
        this.color = color;
    }

    public boolean isSameAs(final String color) {
        return this.color.equals(color);
    }

    public boolean isSameAs(final CarIdentifier c) {
        return this.color.equals(c.color);
    }
}
