package noobbot;

//
// big container for all the data that comes across inthe the game init message
// i don't use it all, but it makes parsing simple
//
public class GameInitData {
    public Race race;

    public class Race {
        public GameInitTrack track;
        public GameInitCars[] cars;
        public GameInitRaceSession raceSession;

        public class GameInitTrack {
            public String id;
            public String name;
            public TrackPiece[] pieces;
            public TrackLane[] lanes;
            public StartingPoint startingPoint;

            public class StartingPoint {
                public GameInitPoint position;
                public double angle;

                public class GameInitPoint {
                    public double x;
                    public double y;
                }
            }
        }

        public class GameInitCars {
            public CarIdentifier id;
            public CarDimensions dimensions;

            public class CarDimensions {
                public double length;
                public double width;
                public double guideFlagPosition;
            }
        }

        public class GameInitRaceSession {
            public int laps;
            public int maxLapTimeMs;
            public boolean quickRace;
        }
    }

    public TrackPiece[] getPieces() {
        return this.race.track.pieces;
    }

    public TrackLane[] getLanes() {
        return this.race.track.lanes;
    }

    public double getCarLength(final int carIndex) {
        return this.race.cars[carIndex].dimensions.length;
    }

    public double getCarLength(final CarIdentifier car) {
        for(GameInitData.Race.GameInitCars c : race.cars) {
            if (car.isSameAs(c.id)) {
                return c.dimensions.length;
            }
        }

        return 0.0;
    }

}


