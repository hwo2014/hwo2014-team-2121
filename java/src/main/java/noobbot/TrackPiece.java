package noobbot;

import com.google.gson.annotations.*;

//
// represent a single piece of track
// TODO: can i reduce all members to 'double' (instead of 'Double')
//
public class TrackPiece {
    @SerializedName("switch") public boolean switchPiece;
    public Double length;
    public Double radius;
    public Double angle;

    // but this *is* called
    TrackPiece() {
        this.radius = null;
        this.angle = null;
        this.length = null;
        this.switchPiece = false;
    }

    //
    // need to check this out carefully:
    // I'm planning to use Gson to read the track info for me; and i'm assuming it leaves len as null when reading curved pieces;   // yep, it does.
    // and leaves radius/angle null for straight pieces    // yes
    // looks like it does not call this constructor during compile => length is not automatically filled in as i wanted. OK. 
    // 
    // i don't think i'm using this
    TrackPiece(final Double length, final Double radius, final Double angle, final boolean switchPiece) {
        this.radius = radius;
        this.angle = angle;
        this.length = length;
        this.switchPiece = switchPiece;
    }

    // just return core length...
    public Double getLength() {
        return getLength(0);
    }

    public Double getLength(final double laneOffset) {
        if (radius != null && angle != null) {
            // positive laneOffset means lane is to right of track center line; negative laneOffset means lane is to left.
            // need to add lane offset for positive turns (lanes to right of center line have smaller radius)
            // but subtract for negative turns
            if (angle > 0) {
                return Math.PI * (radius - laneOffset) * angle / 180.0;
            }
            else {
                return Math.PI * (radius + laneOffset) * -angle / 180.0;
            }
        }
        else {
            return length;
        }
    }

    public boolean isStraight() {
        return (length != null);
    }

    public boolean isSwitchPiece() {
        return switchPiece;
    }

    public Double getRadius(final double laneOffset) {
        if (radius != null && angle != null) {
            // positive laneOffset means lane is to right of track center line; negative laneOffset means lane is to left.
            // need to add lane offset for positive turns (lanes to right of center line have smaller radius)
            // but subtract for negative turns
            if (angle > 0) {
                return (radius - laneOffset);
            }
            else {
                return (radius + laneOffset);
            }
        }
        else {
            return Double.MAX_VALUE;   // straight pieces can be treated as having an indefinitely large radius
        }
    }

    // duplicate of isStraight?
    public Double getTurn() {
        if (angle != null) {
            return angle;
        }
        else {
            return 0.0;  // straight piece has no turn...
        }
    }

    // returns a number that represents how tight a turn is
    public Double getCurvature() {
        if (radius != null && angle != null) {
            return angle/radius;
        }
        else {
            return 0.0;
        }
    }

    public static boolean sameCurvature(TrackPiece a, TrackPiece b) {
        if (a.isStraight()) {
            return (b.isStraight());
        }
        else {
            return !b.isStraight() && (Double.compare(a.radius, b.radius) == 0.0) && (Math.signum(a.angle) == Math.signum(b.angle));
        }
    }

    @Override
    public String toString() {
        if (radius == null) {
            return "|| " + length + " (" + radius + ", " + angle + ")" + " " + switchPiece;
        }
        else if (angle > 0.0) {
            return "(( " + length + ": " + radius + ", " + String.format("%+5.1f", angle) + " (" +  String.format("%3.2f", getLength()) + ")" + " " + switchPiece;
        }
        else {
            return ")) " + length + ": " + radius + ", " + String.format("%+5.1f", angle) + " (" +  String.format("%3.2f", getLength()) + ")" + " " + switchPiece;
        }
    }

}
