package noobbot;

//
// Container for car position information - matches exactly the format/layout used by server, which makes it easy to parse using Gson
//

public class CarPosition {
    public CarIdentifier id;
    public double angle;
    public PiecePosition piecePosition;

    public class PiecePosition {
        public int pieceIndex;
        public double inPieceDistance;
        public LanePosition lane;
        public int lap;

        PiecePosition() {
            pieceIndex = 0;
            inPieceDistance = 0.0;
            lane = new LanePosition();
            lap = 0;
        }

        public class LanePosition {
            public int startLaneIndex;
            public int endLaneIndex;

            LanePosition() {
                startLaneIndex = 0;
                endLaneIndex = 0;
            }
        }
    }

    CarPosition() {
        id = new CarIdentifier();
        angle = 0.0;
        piecePosition = new PiecePosition();
    }


    public double getAngle() {
        return this.angle;
    }

    public int getPieceIndex() {
        return this.piecePosition.pieceIndex;
    }

    public double getInPieceDistance() {
        return this.piecePosition.inPieceDistance;
    }

    public int getLap() {
        return this.piecePosition.lap;
    }

    public int getStartLaneIndex() {
        return this.piecePosition.lane.startLaneIndex;
    }

    public int getEndLaneIndex() {
        return this.piecePosition.lane.endLaneIndex;
    }
}
