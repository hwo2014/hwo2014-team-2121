package noobbot;


//
// represent a single track layout, in terms of TrackPieces and lane offsets
// comes with a bunch of helper/convenience functions
//
public class Track {

    //
    // represent internal information the program generates for use during a race
    //
    private class TrackNotes {
        public double remainingDistance;   // distance to end of curve/straightaway in terms of sums of piece lengths
        public int switchSection;  // index. all pieces with same index are between same switch pieces. provides convenient lookup
        public double switchDistance;  // distance from last switch piece; is zero for all switch pieces

        TrackNotes() {
            this.remainingDistance = 0.0;
        }

        TrackNotes(final double remainingDistance) {
            this.remainingDistance = remainingDistance;
        }

    }

    public TrackPiece[] pieces;
    // lanes are just offsets to the radius
    // lanes are double on the off-chance that GameInit message will deliver fractional lane offsets
    public Double[] lanes;  // working assumption: lane information in GameInit will be indexed from 0 and incrementing though i can easily correct for a non-zero base...
    // maybe safer to read the lane information in as a map? or set? ...Gson *can* do that, it's just a little tricky. Maybe go look that up.
    public TrackNotes[] trackNotes;

    Track(final TrackPiece[] pieces, final Double[] lanes) {
        this.pieces = pieces;
        this.lanes = lanes;
        analyze();
    }

    Track(final TrackPiece[] pieces) {
        this.pieces = pieces;
        this.lanes = null;
        analyze();
    }

    Track() {
        this.pieces = new TrackPiece[0];
        this.lanes = null;
        this.trackNotes = null;
    }

    public double getRemainingDistanceInCurve(final int index) {
        return trackNotes[index].remainingDistance;
    }

    // public double getRemaiddningDistance(final TrackPosition pos) {
    //     return this.getLength(pos.piece, pos.lane) - pos.distance;
    // }

    public TrackPiece getPiece(final int index) {
        return pieces[index];
    }

    public TrackPiece getPieceAfter(final int index) {
        return pieces[ getIndexAfter(index) ];
    }

    public int getIndexAfter(final int index) {
        return (index + 1) % pieces.length;
    }

    public Double getRadius(final int index, final int laneIndex) {
        return pieces[index].getRadius(lanes[laneIndex]);
    }

    public Double getLength(final int index, final int laneIndex) {
        return pieces[index].getLength(lanes[laneIndex]);
    }

    public Double getLength(final int index) {
        return pieces[index].getLength();
    }

    // number of degrees that car will rotate through on driving the entire piece (clockwise turns are positive)
    public Double getTurn(final int index) {
        return pieces[index].getTurn();
    }

    public boolean isStraight(final int index) {
        return pieces[index].isStraight();
    }


    public boolean sameCurvature(final int indexA, final int indexB) {
        return TrackPiece.sameCurvature(pieces[indexA], pieces[indexB]);
    }
    
    //
    // build any extra information
    // 
    public void analyze() {
        int i;

        // get one TrackNotes instance for every piece in track
        trackNotes = new TrackNotes[pieces.length];

        // starting at last piece and working backwards, build up in each trackNotes the distance to the end of that piece's curve
        // for curve meaning, 'contiguous pieces of same curvature'
        i = pieces.length - 1;
        // System.out.println("  i = " + i );
        // System.out.println("  rD = " + trackNotes[i] );
        
        trackNotes[i] = new TrackNotes(getLength(i));
        for (i--; i >= 0; i--) {
            trackNotes[i] = new TrackNotes(getLength(i));
            if ( TrackPiece.sameCurvature(getPiece(i), getPiece(i + 1)) ) {
                // System.out.println("  i = same " + i );
                trackNotes[i].remainingDistance += trackNotes[i + 1].remainingDistance;
            }
        }

        // go through last pieces again in case first and last pieces are part of same curve
        // just add curve len of piece 0 to the last curve on track
        for (i = pieces.length - 1; i >= 0; i--) {
            if ( TrackPiece.sameCurvature(getPiece(i), getPiece(0)) ) {
                trackNotes[i].remainingDistance += trackNotes[0].remainingDistance;
            }
            else {
                break;
            }
        }


        // for each piece in track, set its switchSection
        // by convention,  switch piece gets same index as the pieces after it
        int switchIndex = 0;
        double switchDistance = 0.0;
        for(i = 0; i < trackNotes.length; i++) {
            if (pieces[i].isSwitchPiece()) {
                switchIndex++;
                switchDistance = 0.0;
            }
            trackNotes[i].switchSection = switchIndex;
            trackNotes[i].switchDistance = switchDistance;
            switchDistance += 1.0;  // for purposes of switchDistance, all pieces have length = 1.0
        }

        // wrap around end of array, make sure pieces each side of finsh line get same index
        // NOTE: switchIndex and switchDistance values are carried over from previous loop
        for(i = 0; i < trackNotes.length; i++) {
            if (pieces[i].isSwitchPiece()) {
                break;
            }
            trackNotes[i].switchSection = switchIndex;
            trackNotes[i].switchDistance = switchDistance;
            switchDistance += 1.0;  // for purposes of switchDistance, all pieces have length = 1.0
        }
    }

    //
    // simple distance calculation - assumes a in front of b and a, b on same or adjacent pieces
    // note: does not account for switches in any way.
    // 
    double getSimpleDistance(final CarPosition a, final CarPosition b) {
        double distance;

        distance = a.getInPieceDistance() - b.getInPieceDistance();
        if (a.getPieceIndex() != b.getPieceIndex()) {
            distance += getLength(b.getPieceIndex(), a.getStartLaneIndex());
        }

        return distance;
    }




    @Override
    public String toString() {
        int index;
        String retVal = "{\n";

        for(index = 0; index < this.pieces.length; index++) {
            retVal += " " + String.format("%2d", index) + " " + String.format("%7.2f", trackNotes[index].remainingDistance)
                + " " + String.format("%2d", trackNotes[index].switchSection) + " " + String.format("%3.1f", trackNotes[index].switchDistance) + " ";
            retVal += this.pieces[index].toString();
            retVal += "\n";
        }

        // dump lane information on single line, between square braces
        retVal += "[ ";
        for(index = 0; index < this.lanes.length; index++) {
            retVal += this.lanes[index].toString();
            retVal += ",  ";
        }
        retVal += " ]\n";

        retVal += "}";

        return retVal;
    }

}


    // Calculates distance for a.lane all the way to b, even if b.lane is different. (i.e. b.lane value is ignored in this calculation)
    // a must be the 'newer' position i.e. must be further along the pieces than b
        // if (b.lap == a.lap) {
        //     // System.out.println(">>> lane = " + a.lane + " offset = " + lanes[a.lane] );

        //     for(index = b.piece; index < a.piece; index++) {
        //         // System.out.println(">>> index = " + index  );
        //         // System.out.println(">>> piece = " + pieces[index]  );
        //         distance += this. pieces[index] .getLength(lanes[a.lane]);
        //     }
        // }
        // else {
        //     // assume just that a is on a new lap and b is on previous lap
        //     for(index = b.piece; index < pieces.length; index++) {
        //         distance += this.pieces[index].getLength( lanes[a.lane] );
        //     }
        //     for(index = 0; index < a.piece; index++) {
        //         distance += this.pieces[index].getLength( lanes[a.lane] );
        //     }
        // }


        // // int index;
        // // double distance = a.distance - b.distance;
        // double distance;

        // // looks like, for time being a and b will be no worse than on adjacent pieces (and usually on same piece)
        // // so, assuming position a is ahead of position b, then distance is:

        // if (a.getPieceIndex() == b.getPieceIndex()) {
        //     // a and b are on same piece:
        //     distance = a.getInPieceDistance() - b.getInPieceDistance();
        // }
        // else {
        //     // a,b are on adjacent pieces:
        //     // distance = (this.getPiece(b.piece).getLength(lanes[a.lane]) + a.distance) - b.distance;
        //     distance = (this.getPiece(b.getPieceIndex()).getLength(lanes[a.lane]) + a.getInPieceDistance()) - b.getInPieceDistance();
        // }

